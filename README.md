# blog-angularjs #
Este proyecto esta diseñado para practicar AngularJS + Bower + Grunt.

## Instrucción para descargar el proyecto ##
Para descargar el branch correspondiente al equipo que le corresponda, debe seguir el siguiente comando:

``` 
$ git clone -b group-[group-id] https://gitlab.com/ceal1818/blog-angularjs.git
```

## Instrucciones para versionar los cambios ##

Para compartir los cambios que realice en su respectivo branch debe seguir los siguientes pasos:

* Revisar los cambios que se han realizado (ficheros nuevos, o cambios realizados en ficheros ya versionados).
``` 
$ git status
```

* Iniciar el versionado de todos los cambios que haya realizado en el repositorio local de Git.
``` 
$ git add .
```

* En caso de necesitar versionar ficheros especificos, debe ejecutar el comando add siguiendo la siguiente forma.
``` 
$ git add [file-absolute-path]
```

* Para culminar el versionado de los cambios anteriormente incluidos en el repositorio local. En este proceso puede añadir un mensaje con el parámetro "-m".
``` 
$ git commit -m "[versioning_message]".
```

* Para compartir los cambios en el repositorio remoto, es necesario descargar los posibles cambios que se hayan compartido previamente. En el proceso se pueden detectar conflictos en los cambios. Algunos se resolverán automáticamente, y en otros casos deberá resolverse de forma manual y luego iniciar el proceso de versiona descrito antes.
```  
$ git pull
```

* En cuanto se hayan descargado los cambios y resuelto los conflictos manuales (versionando los cambios por resolución de conflictos), se debe  compartir los cambios con el siguiente comando.
```  
$ git push origin group-[group-id]
```

## Preparación de la base de datos 'mysql-server' ##
Los pasos a seguir para la preparación es la siguiente:

* Habilitar la virtualización en el equipo a través del parametro de virtualización de la BIOS.

* Descargar el OVA desde de la maquina desde este [enlace](https://mega.nz/#!Q2YGhZoS!X7xzdwkfU2kVPBOQIBMoAK8JKkoWg8-2fhcLmvaLeUI). 

* Instalar la última versión de Virtual Box.

* Importar la copia de la VM (fichero OVA) en Virtual Box.

* Acceder a la configuración de la VM y cambiar al directorio puente instalado.

* En cuanto arranque la VM, debemos acceder con el usuario "mysql-server" con password "1234".

* En cuanto entremos en el cuenta del usuario "mysql-server", por favor comprobemos que la maquina virtual se conecta a la misma red que el ordenador host ejecutando el comando ```ifconfig```.

* Esta IP nos servirá para apuntar la aplicación backend a la base de datos de la VM.


## Pasos previos al arranque del ejecicio ##
En cuanto haya descargado el proyecto es necesario descargar las dependencias frontend y backend. Para ello debe seguir los siguientes pasos:

* Entrar en el directorio raíz del proyecto a través de la consola.

* Descargar las dependencias de backend y construcción de la aplicación frontend con npm. Utilice el comando:
```
$ npm install
```

* Descargar las dependencias frontend de la aplicación con bower. Utilice el comando:
```
$ bower install
```

* Reconfigurar la conexión a la base de datos de la maquina virtual hospedada en nuestro equipo, modificando el atributo host del fichero *config/environments.js*.

## Documentación de la API REST ##
Para comprender el funcionamiento, que peticiones debemos construir y que respuestas podemos recibir de la API REST, el proyecto cuenta con el fichero *blog.postman_collection.json*. Este proyecto es una colección documentada de llamadas a la API REST creada en postman.

Para poder utilizar esta herramienta es necesario instalarla en Google Chrome como una  extensión. Para ello consulte la siguiente [página](https://chrome.google.com/webstore/search/postman?utm_source=chrome-ntp-icon).

Y para empezar a utilizar el API REST desde postman, es importante tener en cuenta que el servidor debe estar arrancado y la colección debe ser importada en postman.



