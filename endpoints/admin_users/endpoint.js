var Promise = require('promise'),
	randomstring = require("randomstring"),
	crypto = require("crypto"),
	moment = require('moment');

module.exports = function(app, apiPath) {
	var adminPath = 'admin',
		adminUsersPath = adminPath+'/users',
		adminUserPath = adminUsersPath+'/:userUuid',
		adminUserPostsPath = adminPath+'/users/:userUuid/posts',
		adminUserPostPath = adminUserPostsPath+'/:postUuid';

	/* service: admin/users */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + adminUsersPath,
		callback: function(req, res){
			var userId = (req.userId)? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT u.uuid, u.firstName, u.lastName, u.email, u.enabled, u.createdDate, u.modifiedDate, (a.user_id IS NULL) isAdmin, "
					+"a.birthdate, a.subjects, a.description FROM users u LEFT JOIN authors a ON u.id = a.user_id "
					+"WHERE u.id != ?";	
				
				app.ddbb.query(sql, [userId], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					var _rows = [];
					rows.forEach(function(row){
						var _row = {
							uuid: row.uuid,
							firstName: row.firstName,
							lastName: row.lastName,
							email: row.email,
							enabled: row.enabled,
                            createdDate: row.createdDate,
                            modifiedDate: row.modifiedDate,
							isAdmin: row.isAdmin,
							birthdate: row.birthdate,
							subjects: row.subjects,
							description: row.description
						};
						_rows.push(_row);
					});
					res.status(200);
					res.json(_rows);			
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});

	/* service: admin/users/:userUuid */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + adminUserPath,
		callback: function(req, res){
			var userLoggedId = (req.userId)? req.userId: null;
			var userUuid = req.params.userUuid;

			new Promise(function(success, failed){
				var sql ="SELECT * FROM (SELECT u.uuid, u.firstName, u.lastName, u.email, u.isOnce, "
					+"u.enabled, (a.user_id IS NULL) isAdmin, a.birthdate, a.subjects, "
					+"a.description FROM users u LEFT JOIN authors a ON u.id = a.user_id WHERE u.id != ?) a "
					+"WHERE a.uuid = ?";	
				
				app.ddbb.query(sql, [userLoggedId, userUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					var _row = null;
					rows.forEach(function(row){
						_row = {
							uuid: row.uuid,
							firstName: row.firstName,
							lastName: row.lastName,
							email: row.email,
                            isOnce: row.isOnce,
							enabled: row.enabled,
							isAdmin: row.isAdmin,
							birthdate: row.birthdate,
							subjects: row.subjects,
							description: row.description
						};
					});
					res.status(200);
					res.json(_row);			
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});

	/* service: admin/users*/
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + adminUsersPath,
		callback: function(req, res){
			var user = req.body;

			var uuid = randomstring.generate(10);
			var psswdKey = randomstring.generate(6);
			var cpsswd = null;


			new Promise(function(success, failed){
				var sql ="INSERT INTO users (uuid, firstName, lastName, "
					+"email, enabled, password, isOnce, createdDate, password_key) "
					+"value (?, ?, ?, ?, true, ?, false, NOW(), ?)";	
				
				var cipher = crypto.createCipher("aes256", psswdKey); 
				var cpsswd = cipher.update(user.password, 'utf8', 'hex') + cipher.final('hex');

				app.ddbb.query(
					sql, 
					[uuid, user.firstName, user.lastName, user.email, cpsswd, psswdKey], 
					function(err, rows, fields){
						if (err) {
							failed(err);
						}
						success();
					}
				);

			}).then(
				function(){
					user.uuid = uuid;
					res.status(200);
					res.json({
						state: "success",
						message: "User was created."
					});
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		} 
	});

	/* service: admin/users/:userUuid*/
	app.util.handler({
		verb: 'put',
		endpoint: apiPath + adminUserPath,
		callback: function(req, res){
			var userLoggedId = (req.userId)? req.userId: null;
			var uuid = req.params.userUuid;
			var user = req.body;

			new Promise(function(success, failed){
				var sql ="SELECT id FROM users WHERE uuid = ?",
					params = [uuid];

				app.ddbb.query(sql, params, function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var userId = rows[0].id;
						new Promise(function(success, failed){
							var sql ="UPDATE users SET firstName = ?, lastName = ?, email = ?, "
								+"enabled = ?, modifiedDate = NOW() WHERE uuid = ?",
								params = [
									user.firstName, 
									user.lastName, 
									user.email, 
									user.enabled, 
									uuid
								];

							app.ddbb.query(sql, params, function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success();
							});

						}).then(
							function(){
								if (!user.isAdmin){
									new Promise(function(success, failed){
										var birthdate = (user.birthdate) ? moment(user.birthdate).format("YYYY-MM-DD hh:mm:ss") : null;
										var sql ="UPDATE authors SET birthdate = ?, description = ?, subjects = ? "
											+"WHERE user_id = ?";

										app.ddbb.query(sql, [birthdate, user.description, user.subjects, userId], function(err, rows, fields){
											if (err) {
												failed(err);
											}
											success();
										});
									}).then(
										function(){
											res.status(200);
											res.json(user);	
										},
										function(err){
											console.log(err);
											res.status(500);
											res.send("Error server...");	
										}
									);
								}
								else{
									res.status(200);
									res.json(user);							
								}
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server...");			
							}
						);
					}
					else{
						res.status(404);
						res.send("User not found...");
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server...");			
				}
			);
		}
	});

	/* service: admin/users/:userUuid/posts*/
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + adminUserPostsPath,
		callback: function(req, res){
			var userUuid = req.params.userUuid;
			new Promise(function(success, failed){
				var sql ="SELECT id FROM users WHERE uuid = ?";

				app.ddbb.query(sql, userUuid, function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					if (rows.length > 0){
						var userId = rows[0].id;
						new Promise(function(success, failed){
							var sql ="SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, "
								+"a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, "
								+"(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, "
								+"p.enabled, p.createdDate, p.modifiedDate, p.publishedDate "
								+"FROM posts p JOIN users u ON p.author_id = u.id "
								+"JOIN authors a ON a.user_id = u.id "
								+"WHERE u.id = ? ORDER BY p.publishedDate, countLikes";	
							
							app.ddbb.query(sql, [userId], function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success(rows);
							});

						}).then(
							function(rows){
								var _rows = [];
								
								rows.forEach(function(row){
									var _row = {
										uuid: row.uuid,
										title: row.title,
										content: row.content,
										tags: row.tags.split(","),
										enabled: row.enabled,
										author: {
											uuid: row.author_uuid,
											firstName: row.firstName,
											lastName: row.lastName,
											description: row.description 
										},
										countLikes: row.countLikes,								
										countComments: row.countComments,
										createdDate: row.createdDate,
										modifiedDate: row.modifiedDate,
										publishedDate: row.publishedDate 
									}
									_rows.push(_row);
								});

								res.status(200);
								res.json(_rows);			
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");			
							}
						);
					}
					else{
						res.status(404);
						res.send("User not found...");
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");	
				}
			);
		}
	});

	/* service: admin/users/:userUuid/posts/:postUuid */
	app.util.handler({
		verb: 'put',
		endpoint: apiPath + adminUserPostPath,
		callback: function(req, res){
			var userUuid = req.params.userUuid;
			var postUuid = req.params.postUuid;
			var post = req.body;

			new Promise(function(success, failed){
				var sql ="SELECT id FROM users WHERE uuid = ?";

				app.ddbb.query(sql, userUuid, function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					if (rows.length > 0){
						var userId = rows[0].id;
						new Promise(function(success, failed){
							var sql ="SELECT COUNT(*) countPost FROM posts p "
								+"JOIN users u ON p.author_id = u.id "
								+"JOIN authors a ON a.user_id = u.id "
								+"WHERE u.id = ? and p.uuid = ?";	
							
							app.ddbb.query(sql, [userId, postUuid], function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success(rows);
							});

						}).then(
							function(rows){
								if (rows[0].countPost > 0){
									new Promise(function(success, failed){
										var sql ="UPDATE posts SET enabled = ?, "
											+"modifiedDate = ? WHERE uuid = ?";	
										var modifiedDate = (post.modifiedDate) ? moment(post.modifiedDate).format("YYYY-MM-DD hh:mm:ss") : null;
										
										app.ddbb.query(sql, [post.enabled, modifiedDate, postUuid], function(err, rows, fields){
											if (err) {
												failed(err);
											}
											success();
										});
									}).then(
										function(){
											res.status(200);
											res.json(post);
										},
										function(err){
											console.log(err);
											res.status(500);
											res.send("Error server..");		
										}
									);
								}
								else{
									res.status(404);
									res.send("Post not found...");
								}
				
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");			
							}
						);
					}
					else{
						res.status(404);
						res.send("User not found...");
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");	
				}
			);
		}
	});
}