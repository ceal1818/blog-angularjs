var Promise = require('promise'),
    randomstring = require('randomstring'),
    moment = require('moment');

module.exports = function (app, apiPath) {
    var allPostsPath = 'posts/all',
        allPostPath = allPostsPath + '/:uuid',
        allPostReadedPath = allPostsPath + '/readed',
        allPostCommentsPath = allPostPath + '/comments',
        allPostLikePath = allPostPath + '/like',
        allPostUnlikePath = 'posts/all/:postUuid/unlike',
        allPostCommentLikePath = 'posts/all/:postUuid/comments/:uuid/like',
        allPostCommentUnlikePath = 'posts/all/:postUuid/comments/:uuid/unlike';


    /* service: posts/all */
    app.util.handler({
        verb: 'get',
        endpoint: apiPath + allPostsPath,
        callback: function (req, res) {
            var page = (req.query.page) ? req.query.page : 1;
            var userId = (req.userId) ? req.userId : null;

            var sqlCount = "SELECT count(*) countPost FROM (" +
                "SELECT p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                "FROM posts p JOIN users a ON p.author_id = a.id " +
                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                " UNION " +
                "SELECT p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                "FROM posts p JOIN users a ON p.author_id = a.id " +
                "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                "AND p.public = FALSE" +
                ") a";

            new Promise(function (success, failed) {
                app.ddbb.query(sqlCount, [userId], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });
            }).then(
                function (rows) {
                    var count = rows[0].countPost;

                    if (count > 0) {
                        var start = 0,
                            postsByPage = 10,
                            pages = parseInt(count / postsByPage) + ((count % postsByPage > 0) ? 1 : 0);

                        if (page > pages) {
                            res.status(404);
                            res.json({
                                appCode: 1001,
                                appMessage: "This page doesn't exists."
                            });
                        }

                        if (page > 1 && page <= pages) {
                            start = postsByPage * (page - 1);
                        }

                        new Promise(function (success, failed) {
                            var sql = "SELECT * FROM (" +
                                "SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, a.birthdate, u.email, " +
                                "a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                                "(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, " +
                                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                                "FROM posts p JOIN users u ON p.author_id = u.id " +
                                "JOIN authors a ON a.user_id = u.id " +
                                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                                " UNION " +
                                "SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, a.birthdate, u.email, " +
                                "a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                                "(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, " +
                                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                                "FROM posts p JOIN users u ON p.author_id = u.id " +
                                "JOIN authors a ON a.user_id = u.id " +
                                "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                                "AND p.public = FALSE" +
                                ") a ORDER BY a.publishedDate, a.countLikes DESC LIMIT ?";

                            var params = [userId, userId, userId];

                            if (start > 0) {
                                sql += ", ?";
                                params.push(start);
                            }

                            params.push(postsByPage);

                            app.ddbb.query(sql, params, function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success(rows, fields);
                            });
                        }).then(
                            function (rows, fields) {
                                var _rows = [];
                                rows.forEach(function (row) {
                                    var _row = {
                                        uuid: row.uuid,
                                        title: row.title,
                                        content: row.content,
                                        tags: row.tags.split(","),
                                        enabled: row.enabled,
                                        author: {
                                            uuid: row.author_uuid,
                                            firstName: row.firstName,
                                            lastName: row.lastName,
                                            description: row.description,
                                            email: row.email,
                                            birthdate: row.birthdate
                                        },
                                        isOwner: (row.author_id == userId),
                                        countLikes: row.countLikes,
                                        hasOwnLike: (row.hasOwnLike) ? {
                                            uuid: row.hasOwnLike
                                        } : null,
                                        countComments: row.countComments,
                                        createdDate: row.createdDate,
                                        modifiedDate: row.modifiedDate,
                                        publishedDate: row.publishedDate
                                    }
                                    _rows.push(_row);
                                });

                                res.set("Count-Posts", count);
                                res.set("Posts", postsByPage);
                                res.set("Page", page);
                                res.set("Pages", pages);
                                res.status(200);
                                res.json(_rows);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server..");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1002,
                            appMessage: "There aren't posts registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server..");
                }
            );
        }
    });

    /* service: posts/all/readed */
    app.util.handler({
        verb: 'get',
        endpoint: apiPath + allPostReadedPath,
        callback: function (req, res) {
            var userId = (req.userId) ? req.userId : null;

            var sqlCount = "SELECT count(*) countPost FROM (" +
                "SELECT p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                "FROM posts p JOIN users a ON p.author_id = a.id " +
                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                " UNION " +
                "SELECT p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                "FROM posts p JOIN users a ON p.author_id = a.id " +
                "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                "AND p.public = FALSE" +
                ") a";

            new Promise(function (success, failed) {
                app.ddbb.query(sqlCount, [userId], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });
            }).then(
                function (rows) {
                    var count = rows[0].countPost;

                    if (count > 0) {
                        new Promise(function (success, failed) {
                            var sql = "SELECT * FROM (" +
                                "SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, a.birthdate, u.email, " +
                                "a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                                "(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, " +
                                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate, " +
                                "p.countReaded FROM posts p JOIN users u ON p.author_id = u.id " +
                                "JOIN authors a ON a.user_id = u.id " +
                                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                                " UNION " +
                                "SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, a.birthdate, u.email, " +
                                "a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                                "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                                "(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, " +
                                "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate, " +
                                "p.countReaded FROM posts p JOIN users u ON p.author_id = u.id " +
                                "JOIN authors a ON a.user_id = u.id " +
                                "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                                "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                                "AND p.public = FALSE" +
                                ") a ORDER BY a.countReaded DESC LIMIT 5";

                            var params = [userId, userId, userId];

                            app.ddbb.query(sql, params, function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success(rows, fields);
                            });
                        }).then(
                            function (rows, fields) {
                                var _rows = [];
                                rows.forEach(function (row) {
                                    var _row = {
                                        uuid: row.uuid,
                                        title: row.title,
                                        content: row.content,
                                        tags: row.tags.split(","),
                                        enabled: row.enabled,
                                        author: {
                                            uuid: row.author_uuid,
                                            firstName: row.firstName,
                                            lastName: row.lastName,
                                            description: row.description,
                                            email: row.email,
                                            birthdate: row.birthdate
                                        },
                                        isOwner: (row.author_id == userId),
                                        countLikes: row.countLikes,
                                        hasOwnLike: (row.hasOwnLike) ? {
                                            uuid: row.hasOwnLike
                                        } : null,
                                        countComments: row.countComments,
                                        createdDate: row.createdDate,
                                        modifiedDate: row.modifiedDate,
                                        publishedDate: row.publishedDate
                                    }
                                    _rows.push(_row);
                                });

                                res.status(200);
                                res.json(_rows);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server..");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1002,
                            appMessage: "There aren't posts registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server..");
                }
            );
        }
    });

    /* service: posts/all/:uuid */
    app.util.handler({
        verb: 'get',
        endpoint: apiPath + allPostPath,
        callback: function (req, res) {
            var userId = (req.userId) ? req.userId : null;

            new Promise(function (success, failed) {
                var sql = "SELECT * FROM (" +
                    "SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, a.birthdate, u.email, " +
                    "a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users u ON p.author_id = u.id " +
                    "JOIN authors a ON a.user_id = u.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, a.birthdate, u.email, " +
                    "a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users u ON p.author_id = u.id " +
                    "JOIN authors a ON a.user_id = u.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE" +
                    ") a where a.uuid = ?";

                app.ddbb.query(sql, [userId, userId, userId, req.params.uuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows, fields);
                });
            }).then(
                function (rows, fields) {
                    var _row = null;
                    if (rows.length > 0) {
                        rows.forEach(function (row) {
                            _row = {
                                uuid: row.uuid,
                                title: row.title,
                                content: row.content,
                                tags: row.tags.split(","),
                                enabled: row.enabled,
                                author: {
                                    uuid: row.author_uuid,
                                    firstName: row.firstName,
                                    lastName: row.lastName,
                                    description: row.description,
                                    email: row.email,
                                    birthdate: row.birthdate
                                },
                                isOwner: (row.author_id == userId),
                                countLikes: row.countLikes,
                                hasOwnLike: (row.hasOwnLike) ? {
                                    uuid: row.hasOwnLike
                                } : null,
                                countComments: row.countComments,
                                createdDate: row.createdDate,
                                modifiedDate: row.modifiedDate,
                                publishedDate: row.publishedDate
                            }
                        });
                        /*
                        new Promise(function(success, failed){
                            var sql = "UPDATE ";   
                        }).then(
                            function(rows, fields){

                            },
                            function(error){

                            }
                        );
                        */
                        res.status(200);
                        res.json(_row);
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1003,
                            appMessage: "This posts hasn't registred."
                        });
                    }

                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server..");
                }
            );
        }
    });

    /* service: posts/all/:uuid/comments */
    app.util.handler({
        verb: 'get',
        endpoint: apiPath + allPostCommentsPath,
        callback: function (req, res) {
            var uuid = req.params.uuid;
            var userId = (req.userId) ? req.userId : null;

            new Promise(function (success, failed) {
                var sql = "SELECT * FROM (" +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE " +
                    ") a WHERE a.uuid = ?";
                app.ddbb.query(sql, [userId, uuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });

            }).then(
                function (rows) {
                    if (rows.length > 0) {
                        var postId = rows[0].id;
                        new Promise(function (success, failed) {
                            var sql= "SELECT c.uuid, c.content, u.uuid author_uuid, u.firstName,(SELECT COUNT(*) countLikes FROM likes WHERE comment_id = c.id) countlike,(SELECT UUID FROM likes WHERE comment_id = c.id AND author_id = ?) hasOwnLike,u.lastName, u.id author_id, c.createdDate FROM comments c LEFT OUTER JOIN users u ON c.author_id = u.id WHERE c.post_id = ?"

                            app.ddbb.query(sql, [userId, postId], function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success(rows, fields);
                            });
                        }).then(
                            function (rows, fields) {
                                var _rows = [],
                                    _row = null;

                                rows.forEach(function (row) {                       
                                    var author = null;
                                    if (row.author_uuid) {
                                        author = {
                                            uuid: row.author_uuid,
                                            firstName: row.firstName,
                                            lastName: row.lastName
                                        };
                                    }

                                    _row = {
                                        uuid: row.uuid,
                                        content: row.content,
                                        author: author,
                                        countLikes: row.countlike,
                                        isOwner: (row.author_id == userId),
                                        hasOwnLike: (row.hasOwnLike) ? {
                                            uuid: row.hasOwnLike
                                        } : null,
                                        createdDate: row.createdDate,
                                    }
                                    _rows.push(_row);

                                });
                                res.status(200);
                                res.json(_rows);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server..");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1003,
                            appMessage: "This posts hasn't registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server..");
                }
            );
        }
    });

    /* service: posts/all/:uuid/comments */
    app.util.handler({
        verb: 'post',
        endpoint: apiPath + allPostCommentsPath,
        callback: function (req, res) {
            var postUuid = req.params.uuid;
            var userId = (req.userId) ? req.userId : null;
            var comment = req.body;

            new Promise(function (success, failed) {
                var sql = "SELECT a.id FROM (" +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE" +
                    ") a WHERE a.uuid = ?"
                app.ddbb.query(sql, [userId, postUuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });

            }).then(
                function (rows) {
                    if (rows.length > 0) {
                        var uuid = randomstring.generate(10);
                        var postId = rows[0].id;

                        new Promise(function (success, failed) {
                            var sql = "INSERT INTO comments (uuid, content, post_id, author_id, createdDate) VALUES (?, ?, ?, ?, ?)";
                            var createdDate = (comment.createdDate) ? moment(comment.createdDate).format("YYYY-MM-DD hh:mm:ss") : null;
                            app.ddbb.query(sql, [uuid, comment.content, postId, userId, createdDate], function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success();
                            });
                        }).then(
                            function () {
                                comment.uuid = uuid;
                                comment.author = null;
                                if (userId != null) {
                                    comment.author = {
                                        uuid: req.userUuid,
                                        firstName: req.firstName,
                                        lastName: req.lastName
                                    };
                                }
                                res.status(201);
                                res.json(comment);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server..");
                            }
                        );
                    } else {
                        console.log("Not found...");
                        res.status(404);
                        res.json({
                            appCode: 1003,
                            appMessage: "This posts hasn't registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server..");
                }
            );
        }
    });

    /* service: posts/all/:uuid/like */
    app.util.handler({
        verb: 'post',
        endpoint: apiPath + allPostLikePath,
        callback: function (req, res) {
            var postUuid = req.params.uuid;
            var userId = (req.userId) ? req.userId : null;
            var userUuid = (req.userUuid) ? req.userUuid : null;

            new Promise(function (success, failed) {
                var sql = "SELECT a.id FROM (" +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE" +
                    ") a WHERE a.uuid = ?"
                app.ddbb.query(sql, [userId, postUuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });

            }).then(
                function (rows) {
                    if (rows.length > 0) {
                        var like = {
                            uuid: randomstring.generate(10),
                            userUuid: userUuid,
                            postUuid: postUuid
                        };

                        new Promise(function (success, failed) {
                            var sql = "INSERT INTO likes (uuid, author_id, post_id, createdDate) VALUES (?, ?, ?, now())";
                            app.ddbb.query(sql, [like.uuid, userId, rows[0].id], function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success();
                            });
                        }).then(
                            function () {
                                res.status(200);
                                res.json(like);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server...");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1004,
                            appMessage: "This posts hasn't registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server...");
                }
            );
        }
    });

    /* service: posts/all/:postUuid/unlike */
    app.util.handler({
        verb: 'delete',
        endpoint: apiPath + allPostUnlikePath,
        callback: function (req, res) {
            var postUuid = req.params.postUuid;
            //var likeUuid = req.params.uuid;
            var userId = (req.userId) ? req.userId : null;
            var userUuid = (req.userUuid) ? req.userUuid : null;

            new Promise(function (success, failed) {
                var sql = "SELECT a.id FROM (" +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE" +
                    ") a WHERE a.uuid = ?"
                app.ddbb.query(sql, [userId, postUuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });

            }).then(
                function (rows) {
                    if (rows.length > 0) {
                        var like = {
                            //uuid: likeUuid,
                            userUuid: userUuid,
                            postUuid: postUuid
                        };

                        new Promise(function (success, failed) {

                            var sql = "DELETE FROM likes WHERE author_id = ? AND post_id = (SELECT id FROM posts WHERE UUID = ?)";
                            //var sql = "DELETE FROM likes WHERE uuid = ?";
                            app.ddbb.query(sql, [userId, postUuid], function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success();
                            });
                        }).then(
                            function () {
                                res.status(200);
                                res.json(like);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server...");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1003,
                            appMessage: "This posts hasn't registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server...");
                }
            );
        }
    });

    /* service: posts/all/:postUuid/comments/:uuid/like */
    app.util.handler({
        verb: 'post',
        endpoint: apiPath + allPostCommentLikePath,
        callback: function (req, res) {
            var postUuid = req.params.postUuid,
                commentUuid = req.params.uuid;
            var userId = (req.userId) ? req.userId : null;
            var userUuid = (req.userUuid) ? req.userUuid : null;

            new Promise(function (success, failed) {
                var sql = "SELECT * FROM comments c WHERE c.post_id = (SELECT a.id FROM (" +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE " +
                    ") a WHERE a.uuid = ?) and c.uuid = ?";
                app.ddbb.query(sql, [userId, postUuid, commentUuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });

            }).then(
                function (rows) {
                    if (rows.length > 0) {
                        var commentId = rows[0].id;
                        var like = {
                            uuid: randomstring.generate(10),
                            userUuid: userUuid,
                            commentUuid: commentUuid
                        };
                        new Promise(function (success, failed) {
                            var sql = "INSERT INTO likes (uuid, author_id, comment_id, createdDate) VALUES (?, ?, ?, now())";
                            app.ddbb.query(sql, [like.uuid, userId, commentId], function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success();
                            });
                        }).then(
                            function () {
                                res.status(200);
                                res.json(like);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server...");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1003,
                            appMessage: "This comment hasn't registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server..");
                }
            );
        }
    });

    /* service: posts/all/:postUuid/comments/:uuid/unlike */
    app.util.handler({
        verb: 'delete',
        endpoint: apiPath + allPostCommentUnlikePath,
        callback: function (req, res) {
            var postUuid = req.params.postUuid,
                commentUuid = req.params.uuid;
            //var likeUuid = req.params.likeUuid;
            var userId = (req.userId) ? req.userId : null;
            var userUuid = (req.userUuid) ? req.userUuid : null;

            new Promise(function (success, failed) {
                var sql = "SELECT * FROM comments c WHERE c.post_id = (SELECT a.id FROM (" +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE AND p.public = TRUE " +
                    " UNION " +
                    "SELECT p.id, p.uuid, p.title, p.content, a.uuid author_uuid, a.firstName, a.lastName, " +
                    "p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " +
                    "(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, " +
                    "p.enabled, p.createdDate, p.modifiedDate, p.publishedDate " +
                    "FROM posts p JOIN users a ON p.author_id = a.id " +
                    "JOIN post_readers pr ON p.id = pr.post_id AND pr.reader_id = ? " +
                    "WHERE p.publishedDate IS NOT NULL AND p.enabled = TRUE " +
                    "AND p.public = FALSE " +
                    ") a WHERE a.uuid = ?) and c.uuid = ?";
                app.ddbb.query(sql, [userId, postUuid, commentUuid], function (err, rows, fields) {
                    if (err) {
                        failed(err);
                    }
                    success(rows);
                });

            }).then(
                function (rows) {
                    if (rows.length > 0) {
                        var commentId = rows[0].id;

                        var like = {
                            //uuid: likeUuid,
                            userUuid: userUuid,
                            postUuid: postUuid
                        };

                        new Promise(function (success, failed) {
                            //var sql = "DELETE FROM likes WHERE uuid = ?";
                            var sql = "DELETE FROM likes WHERE author_id = ? AND comment_id = (SELECT id FROM comments WHERE UUID = ?)";
                            app.ddbb.query(sql, [userId, commentUuid], function (err, rows, fields) {
                                if (err) {
                                    failed(err);
                                }
                                success();
                            });
                        }).then(
                            function () {
                                res.status(200);
                                res.json(like);
                            },
                            function (err) {
                                console.log(err);
                                res.status(500);
                                res.send("Error server...");
                            }
                        );
                    } else {
                        res.status(404);
                        res.json({
                            appCode: 1003,
                            appMessage: "This comment hasn't registred."
                        });
                    }
                },
                function (err) {
                    console.log(err);
                    res.status(500);
                    res.send("Error server...");
                }
            );
        }
    });

}
