var Promise = require('promise'),
	moment = require('moment');

module.exports = function(app, apiPath) {
	var endpointPath = 'users';

	app.util.handler({
		verb: 'get',
		endpoint: apiPath + endpointPath,
		callback: function(req, res) {
			var userId = (req.userId)? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT u.uuid, u.firstName, u.lastName, u.email, u.enabled, u.isOnce, (a.user_id IS NULL) "
                    + "isAdmin, a.birthdate, a.subjects, a.description FROM users u LEFT JOIN authors a ON u.id "
                    + "= a.user_id WHERE u.id = ?";

				app.ddbb.query(
					sql, [userId], function(err, rows, fields){
						if (err) {
							failed(err);
						}
						success(rows);
					}
				);
			}).then(
				function(rows){
					if (rows.length > 0){
						res.status(200);
						res.json({
							uuid: rows[0].uuid,
							firstName: rows[0].firstName,
							lastName: rows[0].lastName,
							email: rows[0].email,
							enabled: rows[0].enabled,
							isAdmin: rows[0].isAdmin,
							birthdate: rows[0].birthdate,
							subjects: rows[0].subjects,
							description: rows[0].description,
                            isOnce: rows[0].isOnce
						});				
					}
					else {
						res.status(404);
						res.json({
							appCode: 3001,
							appMessage: "User not found..."
						});
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);	
		}
	});

	app.util.handler({
		verb: 'put',
		endpoint: apiPath + endpointPath,
		callback: function(req, res) {
			var userId = (req.userId)? req.userId: null;
			var user = req.body;

			new Promise(function(success, failed){
				var sql ="SELECT uuid, firstName, lastName, "
					+"email, enabled FROM users WHERE id = ?";

				app.ddbb.query(
					sql, [userId], function(err, rows, fields){
						if (err) {
							failed(err);
						}
						success(rows);
					}
				);
			}).then(
				function(rows){
					if (rows.length > 0){
						new Promise(function(success, failed){
							var sql ="INSERT INTO authors VALUES (?, ?, ?, ?)";
							var birthdate = (user.birthdate) ? moment(user.birthdate).format("YYYY-MM-DD hh:mm:ss") : null;
							app.ddbb.query(
								sql, 
								[userId, birthdate, user.description, user.subjects], 
								function(err, rows, fields){
									if (err) {
										failed(err);
									}
									success();
								}
							);
						}).then(
							function(){
								new Promise(function(success, failed){
									var sql ="UPDATE users SET modifiedDate = NOW(), "
										+"isOnce = FALSE WHERE id = ?";
									app.ddbb.query(sql, [userId], function(err, rows, fields){
										if (err) {
											failed(err);
										}
										success();
									});
								}).then(
									function(){
										res.status(200);
										res.send("User completed...");
									},
									function(err){
										console.log(err);
										res.status(500);
										res.send("Error server..");
									}
								);
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");				
							}					
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 3001,
							appMessage: "User not found..."
						});
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});	
}