var randomstring = require("randomstring"),
	Promise = require('promise'),
	moment = require('moment');

module.exports = function(app, apiPath) {
	var ownPostsPath = 'posts/own',
		ownPostPath = ownPostsPath+'/:uuid',
		ownPostCommentsPath = ownPostPath+'/comments',
		ownPostPublishPath = ownPostPath+'/publish',
		ownPostLikePath = ownPostPath+'/like',
		ownPostUnlikePath = ownPostsPath+'/:postUuid/unlike/:uuid';

	/* service: posts/own */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + ownPostsPath,
		callback: function(req, res) {
			var page = (req.query.page) ? req.query.page: 1;
			var userId = (req.userId) ? req.userId: null;

			var sqlCount = "SELECT count(*) countPost FROM ("
				+"SELECT p.uuid, p.title, p.content, u.uuid author_uuid, u.firstName, u.lastName, "
				+"a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, "
				+"(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, "
				+"p.enabled, p.createdDate, p.modifiedDate, p.publishedDate "
				+"FROM posts p JOIN users u ON p.author_id = u.id "
				+"JOIN authors a ON a.user_id = u.id "
				+"WHERE p.author_id = ?"
				+") a";

			new Promise(function(success, failed){
				app.ddbb.query(sqlCount, [req.userId], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					var count = rows[0].countPost;

					if (count > 0){
						var start = 0, postsByPage = 10,
							pages = parseInt(count / postsByPage) + ((count % postsByPage > 0) ? 1 : 0);

						if (page > pages){
							res.status(404);
							res.json({
								appCode: 2001,
								appMessage: "This page doesn't exists."
							});	
						}

						if (page > 1 && page <= pages){
							start = postsByPage*(page-1);
						}

						new Promise(function(success, failed){
							var sql = "SELECT * FROM ("
								+"SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, "
								+"a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, "
								+"(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, "
								+"(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, "
								+"p.enabled, p.createdDate, p.modifiedDate, p.publishedDate "
								+"FROM posts p JOIN users u ON p.author_id = u.id "
								+"JOIN authors a ON a.user_id = u.id "
								+"WHERE p.author_id = ?"
								+") a ORDER BY a.publishedDate, a.countLikes LIMIT ?";

							var params = [req.userId, req.userId];

							if (start > 0){
								sql += ", ?";
								params.push(start);
							}

							params.push(postsByPage);

							app.ddbb.query(sql, params, function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success(rows, fields);
							});
						}).then(
							function(rows, fields){
								var _rows = [];
								rows.forEach(function(row){
									var _row = {
										uuid: row.uuid,
										title: row.title,
										content: row.content,
										tags: row.tags.split(","),
										enabled: row.enabled,
										author: {
											uuid: row.author_uuid,
											firstName: row.firstName,
											lastName: row.lastName,
											description: row.description 
										},
										isOwner: (row.author_id == userId),
										countLikes: row.countLikes,
										hasOwnLike: (row.hasOwnLike) ? {
											uuid: row.hasOwnLike
										}: null,								
										countComments: row.countComments,
										createdDate: row.createdDate,
										modifiedDate: row.modifiedDate,
										publishedDate: row.publishedDate 
									}
									_rows.push(_row);
								});
								res.set("Count-Posts", count);
								res.set("Posts", postsByPage);
								res.set("Page", page);
								res.set("Pages", pages);
								res.status(200);
								res.json(_rows);
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");
							}
						);
					}
					else{
						res.status(200);
                        res.json([]);
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});

	/* service: posts/own/:uuid */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + ownPostPath,
		callback: function(req, res) {
			var userId = (req.userId) ? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT p.uuid, p.title, p.content, u.id author_id, "
					+"u.uuid author_uuid, u.firstName, u.lastName, a.description, "
					+"p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, " 
					+"(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, "
					+"(SELECT uuid FROM likes WHERE post_id = p.id and author_id = ?) hasOwnLike, "
					+"p.enabled, p.createdDate, p.modifiedDate, p.publishedDate "
					+"FROM posts p JOIN users u ON p.author_id = u.id "
					+"JOIN authors a ON a.user_id = u.id "
					+"WHERE u.id = ? and p.uuid = ?";

				app.ddbb.query(sql, [userId, userId, req.params.uuid], function(err, rows, fields){
						if (err) {
							failed(err);
						}
						success(rows, fields);
					});
			}).then(
				function(rows, fields){
					if (rows.length > 0){
						var _row = null;
						rows.forEach(function(row){
							_row = {
								uuid: row.uuid,
								title: row.title,
								content: row.content,
								tags: row.tags.split(","),
								enabled: row.enabled,
								author: {
									uuid: row.author_uuid,
									firstName: row.firstName,
									lastName: row.lastName,
									description: row.description
								},
								isOwner: (row.author_id == userId),
								countLikes: row.countLikes,
								hasOwnLike: (row.hasOwnLike) ? {
									uuid: row.hasOwnLike
								}: null,
								countComments: row.countComments,
								createdDate: row.createdDate,
								modifiedDate: row.modifiedDate,
								publishedDate: row.publishedDate 
							}
						});
						res.status(200);
						res.json(_row);
					} 
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}		
			);
		}
	});	

	/* service: posts/own */
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + ownPostsPath,
		callback: function(req, res) {
			var uuid = randomstring.generate(10);
			var post = req.body;
			var userId = req.userId;
			
			new Promise(function(success, failed){
				var sql = "INSERT INTO posts (uuid, author_id, title, content, enabled, createdDate, tags) VALUES (?, ?, ?, ?, ?, ?, ?)";
				var createdDate = (post.createdDate) ? moment(post.createdDate).format("YYYY-MM-DD hh:mm:ss") : null;
				//var publishedDate = (post.publishedDate) ? moment(post.publishedDate).format("YYYY-MM-DD hh:mm:ss") : null;

				app.ddbb.query(sql, [uuid, userId, post.title, post.content, post.enabled, createdDate, post.tags.toString()], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success();

				});
			}).then(
				function(){
					post.uuid = uuid;
					res.status(201);
					res.json(post);
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}
			);
		}
	});

	/* service: posts/own/:uuid */
	app.util.handler({
		verb: 'put',
		endpoint: apiPath + ownPostPath,
		callback: function(req, res) {
			var post = req.body;
			var uuid = req.params.uuid;

			new Promise(function(success, failed){
				var sql = "SELECT count(*) as count FROM posts WHERE posts.uuid = ?";
				
				app.ddbb.query(sql, [uuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					var sql = "UPDATE posts SET title = ?, content = ?, enabled = ?, tags = ?, modifiedDate = ? WHERE uuid = ?";
					var modifiedDate = (post.modifiedDate) ? moment(post.modifiedDate).format("YYYY-MM-DD hh:mm:ss") : null;

					if (rows[0].count > 0){
						new Promise(function(success, failed){
							app.ddbb.query(
								sql, 
								[post.title, post.content, post.enabled, post.tags.toString(), modifiedDate, uuid], 
								function(err, rows, fields){
									if (err) {
										failed(err);
									}
									success();
								});
						}).then(
							function(){
								res.status(200);
								res.json(post);
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");
							}
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}
			);
		}
	});	

	/* service: posts/own/:uuid */
	app.util.handler({
		verb: 'delete',
		endpoint: apiPath + ownPostPath,
		callback: function(req, res) {
			var postUuid = req.params.uuid;
			var userId = (req.userId)? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT p.id FROM posts p JOIN users u ON "
					+"p.author_id = u.id WHERE u.id = ? and p.uuid = ?";

				app.ddbb.query(sql, [userId, postUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					if (rows.length > 0){
						var postId = rows[0].id;
						/* Fin del borrado de post_readers del Post */
						new Promise(function(success, failed){
							var sql ="SELECT post_id FROM post_readers WHERE post_id = ?";
							app.ddbb.query(sql, [postId], function(err, rowsPostReaders, fields){
								if (err) {
									failed(err);
								}
								success(rowsPostReaders);
							});
						}).then(function(rowsPostReaders){
							if (rowsPostReaders.length > 0){
								new Promise(function(success, failed){
									var sql = "DELETE FROM post_readers WHERE post_id = ?";
									app.ddbb.query(sql, [postId], function(err, rowsPostReaders, fields){
										if (err) {
											failed(err);
										}
										success();
									});
								}).then(
									function(){
										res.status(200);
									},
									function(err){
										console.log(err);
										res.status(500);
										res.send("Error server in delete post_readers...");
									}
								);
							}
						},
						function(err){
							console.log(err);
							res.status(500);
							res.send("Error server in select post_readers...");
						});
						/* Fin del borrado de post_readers del Post */
						/* Inicio del borrado de likes del Post */
						new Promise(function(success, failed){
							var sql ="SELECT post_id FROM likes WHERE post_id= ?";
							app.ddbb.query(sql, [postId], function(err, rowsLikes, fields){
								if (err) {
									failed(err);
								}
								success(rowsLikes);
							});
						}).then(function(rowsLikes){
							if (rowsLikes.length > 0){
								new Promise(function(success, failed){
									var sql = "DELETE FROM likes WHERE post_id = ?";
									app.ddbb.query(sql, [postId], function(err, rowsLikes, fields){
										if (err) {
											failed(err);
										}
										success();
									});
								}).then(
									function(){
										//res.status(200);
									},
									function(err){
										console.log(err);
										res.status(500);
										res.send("Error server in delete likes...");
									}
								);
							}
						},
						function(err){
							console.log(err);
							res.status(500);
							res.send("Error server select likes...");
						});
						/* Fin del borrado de likes del Post */
						/* Inicio del borrado de comentarios del Post */
						new Promise(function(success, failed){
							var sql ="SELECT post_id FROM comments WHERE post_id= ?";
							app.ddbb.query(sql, [postId], function(err, rowsComments, fields){
								if (err) {
									failed(err);
								}
								success(rowsComments);
							});
						}).then(
							function(rowsComments){
								if (rowsComments.length > 0){
									new Promise(function(success, failed){
										var sql = "DELETE FROM comments WHERE post_id = ?";
										app.ddbb.query(sql, [postId], function(err, rowsComments, fields){
											if (err) {
												failed(err);
											}
											success();
										});
									}).then(
										function(){
											//res.status(200);
										},
										function(err){
											console.log(err);
											res.status(500);
											res.send("Error server in delete comments...");
										}
									);
								}
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server in select comments...");
							}
						);
						/* Fin borrado de comentarios del Post*/
						/* Borrar el Post */
						new Promise(function(success, failed){
							var sql = "DELETE FROM posts WHERE author_id = ? AND id = ?";
										
							app.ddbb.query(sql, [userId, postId], function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success();
							});
						}).then(
							function(){
								res.status(200);
								res.json(post);
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server in delete post...");
							}
						);
					} else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server...");
				}
			);
		}
	});

	/* service: posts/own/:uuid/comments */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + ownPostCommentsPath,
		callback: function(req, res) {
			var uuid = req.params.uuid;
			var userId = (req.userId)? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT p.id FROM posts p JOIN users u ON p.author_id = u.id "
					+"WHERE u.id = ? and p.uuid = ?";
				
				app.ddbb.query(sql, [userId, uuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var postId = rows[0].id;
						new Promise(function(success, failed){
							var sql = "SELECT c.uuid, c.content, u.uuid author_uuid, u.firstName, u.lastName, c.createdDate FROM comments c LEFT OUTER JOIN users u ON c.author_id = u.id WHERE c.post_id = ?";

							app.ddbb.query(sql, [postId], function(err, rows, fields){
									if (err) {
										failed(err);
									}
									success(rows, fields);
								});
						}).then(
							function(rows, fields){
								var _rows = [],
									_row = null;

								rows.forEach(function(row){
									var author = null;

									if (row.author_uuid){
										author = {
											uuid: row.author_uuid,
											firstName: row.firstName,
											lastName: row.lastName 
										};
									}

									_row = {
										uuid: row.uuid,
										content: row.content,
										author: author,
										countLikes: row.countLikes,
										createdDate: row.createdDate,
									}
									_rows.push(_row);
								});
								res.status(200);
								res.json(_rows);
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");
							}		
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}		
			);
		}
	});	

	/* service: posts/own/:uuid/comments */
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + ownPostCommentsPath,
		callback: function(req, res) {
			var postUuid = req.params.uuid;
			var userId = (req.userId)? req.userId: null;
			var comment = req.body;

			new Promise(function(success, failed){
				var sql ="SELECT p.id FROM posts p JOIN users u ON p.author_id = u.id "
					+"WHERE u.id = ? and p.uuid = ?";	
				
				app.ddbb.query(sql, [userId, postUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var uuid = randomstring.generate(10);
						var postId = rows[0].id;

						new Promise(function(success, failed){
							var sql = "INSERT INTO comments (uuid, content, post_id, author_id, createdDate) VALUES (?, ?, ?, ?, ?)";
							var createdDate = (comment.createdDate) ? moment(comment.createdDate).format("YYYY-MM-DD hh:mm:ss") : null;
										
							app.ddbb.query(sql, [uuid, comment.content, postId, userId, createdDate], function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success();
							});
						}).then(
							function(){
								comment.uuid = uuid;
								comment.author = null;
								if (userId != null){
									comment.author = {
										uuid: req.userUuid,
										firstName: req.firstName,
										lastName: req.lastName
									};
								}						
								res.status(201);
								res.json(comment);						
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");
							}
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}	
			);
		}
	});

	/* service: posts/own/:uuid/like */
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + ownPostLikePath,
		callback: function(req, res) {
			var postUuid = req.params.uuid;
			var userId = (req.userId)? req.userId: null;
			var userUuid = (req.userUuid)? req.userUuid: null;

			new Promise(function(success, failed){
				var sql ="SELECT p.id FROM posts p JOIN users u ON "
					+"p.author_id = u.id WHERE u.id = ? and p.uuid = ?";	
				
				app.ddbb.query(sql, [userId, postUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var like = {
							uuid: randomstring.generate(10),
							userUuid: userUuid,
							postUuid: postUuid
						};

						new Promise(function(success, failed){
							var sql = "INSERT INTO likes (uuid, author_id, post_id, createdDate) VALUES (?, ?, ?, now())";
										
							app.ddbb.query(sql, [like.uuid, userId, rows[0].id], function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success();
							});
						}).then(
							function(){
								res.status(200);
								res.json(like);			
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server...");
							}
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server...");
				}
			);
		}
	});

	/* service: posts/own/:postUuid/unlike/:uuid */
	app.util.handler({
		verb: 'delete',
		endpoint: apiPath + ownPostUnlikePath,
		callback: function(req, res) {
			var postUuid = req.params.postUuid;
			var likeUuid = req.params.uuid;
			var userId = (req.userId)? req.userId: null;
			var userUuid = (req.userUuid)? req.userUuid: null;

			new Promise(function(success, failed){
				var sql ="SELECT p.id FROM posts p JOIN users u ON "
					+"p.author_id = u.id WHERE u.id = ? and p.uuid = ?";	
				
				app.ddbb.query(sql, [userId, postUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var like = {
							uuid: likeUuid,
							userUuid: userUuid,
							postUuid: postUuid
						};

						new Promise(function(success, failed){
							var sql = "DELETE FROM likes WHERE author_id = ? AND post_id = ?";
										
							app.ddbb.query(sql, [userId, rows[0].id], function(err, rows, fields){
								if (err) {
									failed(err);
								}
								success();
							});
						}).then(
							function(){
								res.status(200);
								res.json(like);			
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server...");
							}
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server...");
				}
			);
		}
	});

	/* service: /posts/own/:uuid/publish */
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + ownPostPublishPath,
		callback: function(req, res) {
			var postUuid = req.params.uuid;
			var userId = (req.userId)? req.userId: null;
			var publishObj = req.body;

			new Promise(function(success, failed){
				var sql ="SELECT p.id FROM posts p JOIN users u ON p.author_id = u.id "
					+"WHERE u.id = ? and p.uuid = ? and p.publishedDate is null";	
				
				app.ddbb.query(sql, [userId, postUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var postId = rows[0].id;
						var sql = "UPDATE posts SET publishedDate = ?, enabled = true, "
							+"public = ? WHERE uuid = ?";
                        
						new Promise(function(success, failed){
							app.ddbb.query(
								sql, 
								[
                                    moment(publishObj.publishedDate).format("YYYY-MM-DD hh:mm:ss"), 
									(publishObj.type == "public"), 
									postUuid
								], 
								function(err, rows, fields){
									if (err) {
										failed(err);
									}
									success();
								}
							);
						}).then(
							function(){
								if (publishObj.type != "public"){
									new Promise(function(parentSuccess, parentFailed){
										var usersCount = publishObj.users.length;
										var errors = [];
										var count = 1;
										for (var index = 0; index < publishObj.users.length; index++){
											var readerUuid = publishObj.users[index];

											new Promise(function(success, failed){
												var query = "SELECT u.id, u.uuid FROM (SELECT u.id, u.uuid FROM users u "
													+"JOIN authors a ON u.id = a.user_id WHERE u.id != ?) u WHERE "
													+"u.uuid = ? AND (SELECT COUNT(*) FROM post_readers pr WHERE "
													+"pr.reader_id = u.id AND pr.post_id = ?) = 0";
												
												app.ddbb.query(query, [userId, readerUuid, postId], function(err, rows, fields){
													if (err) {
														failed(err);
													}
													success(rows);
												});
											}).then(
												function(rows){
													if (rows.length > 0){
														new Promise(function(success, failed){
															var query = "INSERT INTO post_readers VALUES (?, ?, NOW())";
												
															app.ddbb.query(query, [postId, rows[0].id], function(err, rows, fields){
																if (err) {
																	failed(err);
																}
																success();
															});
														}).then(
															function(){
																if (count == usersCount){
																	parentSuccess(publishObj);
																}
																count++;
															},
															function(err){
																errors.push(err);
																if (count == usersCount){
																	parentFailed(errors);
																}
																count++;
															}
														);
													}
													else{
														if (count == usersCount){
															parentSuccess(publishObj);
														}
														count++;
													}
												},
												function(err){
													errors.push(err);
													if (count == usersCount){
														parentFailed(errors);
													}
													count++;							
												}
											);
										}
									}).then(
										function(rows){
											res.status(200);
											res.json(req.body);
										},
										function(err){
											console.log(err);
											res.status(500);
											res.send("Error server..");
										}
									);
								}
								else{
									res.status(200);
									res.json(req.body);							
								}
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");
							}
						);
					}
					else {
						res.status(404);
						res.json({
							appCode: 2003,
							appMessage: "This posts hasn't registred."
						});					
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}		
			);
		}
	});
}