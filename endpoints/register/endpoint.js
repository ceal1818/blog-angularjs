var randomstring = require("randomstring"),
	Promise = require('promise'),
	crypto = require("crypto");

module.exports = function(app, apiPath) {
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + 'register',
		callback: function(req, res) {
			var user = req.body;

			var uuid = randomstring.generate(10);
			var psswdKey = randomstring.generate(6);
			var cpsswd = null;

			new Promise(function(success, failed){
				var sql ="INSERT INTO users (uuid, firstName, lastName, "
					+"email, enabled, password, isOnce, createdDate, password_key) "
					+"value (?, ?, ?, ?, true, ?, true, NOW(), ?)";	
				
				var cipher = crypto.createCipher("aes256", psswdKey); 
				var cpsswd = cipher.update(user.password, 'utf8', 'hex') + cipher.final('hex');

				app.ddbb.query(
					sql, 
					[uuid, user.firstName, user.lastName, user.email, cpsswd, psswdKey], 
					function(err, rows, fields){
						if (err) {
							failed(err);
						}
						success();
					}
				);

			}).then(
				function(){
					user.uuid = uuid;
					res.status(200);
					res.json({
						state: "success",
						message: "User was created."
					});
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});
}