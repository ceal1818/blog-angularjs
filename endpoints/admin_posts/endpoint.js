var Promise = require('promise'),
	moment = require('moment');

module.exports = function(app, apiPath) {
	var adminPath = 'admin',
		adminPostsPath = adminPath+'/posts',
		adminPostPath = adminPostsPath+'/:postUuid';

	/* service: admin/posts */ 
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + adminPostsPath,
		callback: function(req, res){
			var userId = (req.userId)? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, "
					+"a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, "
					+"(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, "
					+"p.enabled, p.createdDate, p.modifiedDate, p.publishedDate "
					+"FROM posts p JOIN users u ON p.author_id = u.id "
					+"JOIN authors a ON a.user_id = u.id "
					+"ORDER BY p.publishedDate, countLikes";	
				
				app.ddbb.query(sql, function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					var _rows = [];
					
					rows.forEach(function(row){
						var _row = {
							uuid: row.uuid,
							title: row.title,
							content: row.content,
							tags: row.tags.split(","),
							enabled: row.enabled,
							author: {
								uuid: row.author_uuid,
								firstName: row.firstName,
								lastName: row.lastName,
								description: row.description 
							},
							countLikes: row.countLikes,								
							countComments: row.countComments,
							createdDate: row.createdDate,
							modifiedDate: row.modifiedDate,
							publishedDate: row.publishedDate 
						}
						_rows.push(_row);
					});

					res.status(200);
					res.json(_rows);			
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});	

	/* service: admin/posts/:postUuid */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + adminPostPath,
		callback: function(req, res){
			var userLoggedId = (req.userId)? req.userId: null;
			var postUuid = req.params.postUuid;

			new Promise(function(success, failed){
				var sql ="SELECT p.uuid, p.title, p.content, u.id author_id, u.uuid author_uuid, u.firstName, u.lastName, "
					+"a.description, p.tags, (SELECT COUNT(*) FROM comments WHERE post_id = p.id) countComments, "
					+"(SELECT COUNT(*) FROM likes WHERE post_id = p.id) countLikes, "
					+"p.enabled, p.createdDate, p.modifiedDate, p.publishedDate "
					+"FROM posts p JOIN users u ON p.author_id = u.id "
					+"JOIN authors a ON a.user_id = u.id "
					+"WHERE p.uuid = ?"
					+"ORDER BY p.publishedDate, countLikes";	
				
				app.ddbb.query(sql, [postUuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					if (rows.length > 0){
						var _row = null;
						rows.forEach(function(row){
							_row = {
								uuid: row.uuid,
								title: row.title,
								content: row.content,
								tags: row.tags.split(","),
								enabled: row.enabled,
								author: {
									uuid: row.author_uuid,
									firstName: row.firstName,
									lastName: row.lastName,
									description: row.description 
								},
								countLikes: row.countLikes,								
								countComments: row.countComments,
								createdDate: row.createdDate,
								modifiedDate: row.modifiedDate,
								publishedDate: row.publishedDate 
							}
						});
						res.status(200);
						res.json(_row);	
					}
					else{
						res.status(404);
						res.send("User not found...");				
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});

	/* service: admin/posts/:postUuid */
	app.util.handler({
		verb: 'put',
		endpoint: apiPath + adminPostPath,
		callback: function(req, res){
			var post = req.body;
			var uuid = req.params.postUuid;

			new Promise(function(success, failed){
				var sql = "SELECT count(*) as count FROM posts WHERE posts.uuid = ?";
				
				app.ddbb.query(sql, [uuid], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					var sql = "UPDATE posts SET enabled = ?, modifiedDate = ? WHERE uuid = ?";
					var modifiedDate = (post.modifiedDate) ? moment(post.modifiedDate).format("YYYY-MM-DD hh:mm:ss") : null;

					if (rows[0].count > 0){
						new Promise(function(success, failed){
							app.ddbb.query(
								sql, 
								[post.enabled, modifiedDate, uuid], 
								function(err, rows, fields){
									if (err) {
										failed(err);
									}
									success();
								});
						}).then(
							function(){
								res.status(200);
								res.json(post);
							},
							function(err){
								console.log(err);
								res.status(500);
								res.send("Error server..");
							}
						);
					}
					else{
						res.status(404);
						res.send("Post not found..");
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}
			);
		}
	});

}