var randomstring = require("randomstring"),
	Promise = require('promise'),
	crypto = require("crypto");

module.exports = function(app, apiPath) {
	app.util.handler({
		verb: 'post',
		endpoint: apiPath + 'login',
		callback: function(req, res) {
			var login = req.body;

			new Promise(function(success, failed){
				var sql = "SELECT id, uuid, firstName, lastName, isOnce, "
					+"password, password_key psswdKey FROM users WHERE "
					+"email = ? AND enabled = TRUE";	
				
				app.ddbb.query(sql, [login.email], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});
			}).then(
				function(rows){
					if (rows.length > 0){
						var psswdKey = rows[0].psswdKey,
							password = rows[0].password,
							userId = rows[0].id;

						var cipher = crypto.createCipher("aes256", psswdKey); 
						var cpsswd = cipher.update(login.password, 'utf8', 'hex') + cipher.final('hex');

						if (password == cpsswd){
							var cipher2 = crypto.createCipher("aes256", psswdKey);
								token = randomstring.generate(12), 
								tokenSeparator = randomstring.generate({
		 							length: 3, 
		 							charset: '$?!'
		 						}),
								userAgent = (req.headers['user-agent']) ? req.headers['user-agent']: '',
								ip = req.ip;

							var _token = token+tokenSeparator+ip,
								xToken = cipher2.update(_token, 'utf8', 'hex') + cipher2.final('hex');

							new Promise(function(success, failed){
								var sql = "INSERT INTO security_tokens "
									+"(user_id, access_date, token, token_sep, "
									+"ip, user_agent, x_token) VALUES (?, NOW(), ?, ?, ?, ?, ?)";	
								
								app.ddbb.query(
									sql, 
									[userId, token, tokenSeparator, ip, userAgent, xToken], 
									function(err, rows, fields){
										if (err) {
											failed(err);
										}
										success();
									}
								);
							}).then(
								function(){
									res.status(200);
									res.json({
										xToken: xToken,
										isOnce: rows[0].isOnce
									});
								},
								function(err){
									console.log(err);
									res.status(500);
									res.send("Error server..");
								}
							);
						}
						else{
							res.status(401);
							res.send("User unauthorized...");	
						}
					}
					else{
						res.status(404);
						res.send("User invalid...");		
					}
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");
				}
			);
		}
	});
}