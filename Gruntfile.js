module.exports = function (grunt) {
    grunt.initConfig({

        clean: {
            build: "build"
        },

        copy: {
            
            css:{
                expand: true,
                flatten: true,
                src: [
                    "bower_components/bootstrap/dist/css/bootstrap.css",
                    "bower_components/bootstrap/dist/css/bootstrap-theme.css"
                ],
                dest: "build/css/"
            },
            
            js:{
                expand: true,
                src: [
                    "application/**/*.js"
                ],
                dest: "build/"
            },         
            
            lib:{
                expand: true,
                flatten: true,
                src: [
                    "bower_components/jquery/dist/jquery.min.js",
                    "bower_components/bootstrap/dist/js/bootstrap.min.js",
                    "bower_components/angular/angular.min.js",
                    "bower_components/angular-route/angular-route.min.js"
                ],
                dest: "build/lib/"
            },
            
            main:{
                expand: true,
                flatten: true,
                src: [
                    "application/index.html"
                ],
                dest: "build/"
            },            
            
            templates:{
                expand: true,
                src: [
                    "application/*/templates/*.html"
                ],
                dest: "build/"
            }             
        },
        
        concat:{
            
            css:{
                src: [
                    "bower_components/bootstrap/dist/css/bootstrap.css",
                    "bower_components/bootstrap/dist/css/bootstrap-theme.css"
                ],
                dest: 'build/css/app.css'
            },
            
        },
        
        express: {
            dev: {
                options: {
                    port: 3000,
                    script: "index.js"
                }
            },
            pro: {
                options: {
                    port: 80,
                    script: "index.js",
                    background: false
                }
            },            
        },

        watch: {
            express: {
                files: ['build/**/*.*'],
                tasks: ['express:dev'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        }

    });

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-express-server");
    grunt.loadNpmTasks("grunt-contrib-watch");    

    grunt.registerTask("default", [
        "clean:build",
        "copy:css",
        "copy:js",
        "copy:lib",
        "copy:main",
        "copy:templates"
    ]);

    grunt.registerTask("generate", [
        "copy:css",
        "copy:js",
        "copy:templates"
    ]);    
    
    grunt.registerTask("serve:dev", [
        "express:dev",
        "watch"
    ]);
    
    grunt.registerTask("serve", [
        "express:pro"
    ]);    
};